// -*- js -*-
import ItemSheetDuneTalent from "../../item/sheets/talent.js"
import CompositeActorSheet from "./composite.js"
import * as utls from "../utils.js"

export default class ActorSheetDuneCharacter extends CompositeActorSheet {
    
    /** @override */
    get template() {
	if ( !game.user.isGM && this.actor.limited )
	    return "systems/dune/templates/actors/limited-sheet.html";
	let t;
	switch (this.actor.data.type) {
	case "Asset": t = "asset"; break;
	case "House": t = "house"; break;
	case "Supporting Character":
	case "Non-Player Character":
	    this.npcNoEdit = !game.user.isGM;
	default: t =	"character";
	}
	return `systems/dune/templates/actors/${t}-sheet.html`;
    }

    /** @override */
    get isEditable() {
	return super.isEditable && !this.npcNoEdit;
    }

    getData() {
	const data = super.getData();
	data.talentCount = this.actor.items.filter(i => i.data.type == 'talent').length;
	return data;
    }

    /** @override */
    static get defaultOptions() {
	return mergeObject(super.defaultOptions, {
	    //scrollY: [".tab.main"],
	    tabs: [{navSelector: ".tabs", contentSelector: ".sheet-body", initial: "main"}]
	});
    }
    
  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   * @override
   */
    activateListeners(html) {
	super.activateListeners(html);
	const softLock = this.actor.data.data.softLock;
	const editable = this.isEditable && !softLock;
	if (editable) {
	    html.find(".add-trait").click(this._addTrait.bind(this));
	    html.find(".add-asset").click(this._addAsset.bind(this));
	    html.find(".trait button").click(this._removeTraitAsset.bind(this));
	    html.find(".asset button").click(this._removeTraitAsset.bind(this));
	    html.find(".talent button.del").click(this._removeTalent.bind(this));
	    html.find(".talent button.edit").click(this._editTalent.bind(this));
	}
	if (softLock) {
	    html.find("input").attr("disabled", true);
	    html.find("button").attr("disabled", true);
	}
	html.find(".talent b").click(e => html.find($(e.target).attr("data-toggle")).toggle());
	html.find(".soft-lock").click(e => this.actor.update({ "data.softLock": !this.actor.data.data.softLock }));

	// allow pictures to grow
	if (html.hasClass("locked")) {
	    html.find("header.sheet-header img.profile").click(e => $(e.target).toggleClass("bigger"));
	}
    }

    _addTrait() {
	this.actor.createEmbeddedDocuments("Item",
					   [{ type: "trait", name: "trait", data: {temporary: true} }],
					   {renderSheet: false});
    }

    _removeTraitAsset(e) {
	let id = $(e.target).attr("data");
        if (!id)
            id = $(e.target).parent().attr("data");
        let item = this.actor.items.get(id);
	if (item.data.data.temporary)
	    this.actor.deleteEmbeddedDocuments("Item", [id]);
	else
            utls.confirmDelete(() => this.actor.deleteEmbeddedDocuments("Item", [id]), item);
    }

    _addAsset() {
	this.actor.createEmbeddedDocuments("Item",
					   [{ type: "asset", name: "asset", data: {temporary: true, quality: 0} }],
					   {renderSheet: false});
    }

    _removeTalent(e) {
	let id = $(e.target).attr("data");
	if (!id)
	    id = $(e.target).parent().attr("data");
	let item = this.actor.items.get(id);
	utls.confirmDelete(() => this.actor.deleteEmbeddedDocuments("Item", [id]), item);
    }

    _editTalent(e) {
	let id = $(e.target).attr("data");
	if (!id)
	    id = $(e.target).parent().attr("data");
	let item = this.actor.items.get(id);
	new ItemSheetDuneTalent(item).render(true);
    }

}
