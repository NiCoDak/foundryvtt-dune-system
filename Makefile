SRC=LICENSE README.md lang modules packs scripts styles template.json templates

build: _build/system.json
	cp -a $(SRC) _build/

_build:
	mkdir -p $@
_build/system.json: _build
	test -n "$(TAG)"
	test -n "$(MANIFEST)"
	test -n "$(PKG_U)"
	jq < system.json  '.version = "$(TAG)"' |\
		jq '.manifest = "$(MANIFEST)"' |\
		jq '.download =  "$(PKG_U)"' > $@
	if [ -n "$(CI_PROJECT_URL)" ]; then \
		mv $@ $@.n ; \
		jq < $@.n '.url = "$(CI_PROJECT_URL)"' > $@ ;\
		rm -f $@.n ; \
	fi

clean:
	rm -Rf _build
